# coding: utf8

import os
import sys
import logging
from argparse import (
    ArgumentParser,
    RawTextHelpFormatter
)

from chocospoof.loggers import init_logging, enable_verbose, enable_debug
from chocospoof.actions import (
    ping,
    pingscan,
    dnsspoof,
    arpspoof,
    arpunspoof,
    arpmitm,
)

logger = logging.getLogger(__name__)


_DESCRIPTION = '''         {
      {   }                                                                      
       }_{ __{                                                                   
    .-{   }   }-.                                                                
   (   }     {   )                                                               
   |`-.._____..-'|             _                                            __   
   |             ;--.      ___| |__   ___   ___ ___  ___ _ __   ___   ___  / _|  
   |            (__  \    / __| '_ \ / _ \ / __/ _ \/ __| '_ \ / _ \ / _ \| |_   
   |             | )  )  | (__| | | | (_) | (_| (_) \__ | |_) | (_) | (_) |  _|  
   |             |/  /    \___|_| |_|\___/ \___\___/|___| .__/ \___/ \___/|_|    
   |             /  /                                   |_|                      
   |            (  /                                                             
   \             '                                                               
    `-.._____..-'                                                                
''' # noqa: W291, W605, E501, E261


_EPILOG = '''Examples :

  1. Ping host
  %(prog)s ping 192.168.1.1

  2. Scan network with pings
  %(prog)s pingscan 192.169.1.0/24

  3. Scan network with pings, faster
  %(prog)s pingscan 192.168.1.0/24 -M 128

  4. ARP spoof
  %(prog)s arpspoof 192.168.1.1 -g 8.8.8.8
     Victim is 192.168.1.1.
     Spoofed IP is -s 8.8.8.8.
     This will add 8.8.8.8 <> YOUR_MAC_ADDRESS entry in victim's ARP table.
  %(prog)s arpunspoof 192.168.1.1 -s 8.8.8.8
     This undo the attack.

  5. ARP man-in-the-middle
  %(prog)s arpmitm 192.168.1.1 -g 192.168.1.254
     Victim is 192.168.1.1.
     Spoofed gateway IP is -g 192.168.1.254.
     This will poison both ARP tables (on victim and gateway).
     Thus performing the MITM attack.

  6. DNS spoof
  %(prog)s -v dnsspoof 192.168.1.5 192.168.1.10 -g 192.168.1.1 -g google.com
    Attacker 192.168.1.5
    Victim 192.168.1.10
    Gateway 192.168.1.1
    Redirected domain name google.com
    The name google.com will be redirect to the attacker IP
'''


def main():
    parser = ArgumentParser(prog='chocospoof',
                            formatter_class=RawTextHelpFormatter,
                            description=_DESCRIPTION,
                            epilog=_EPILOG)
    parser.add_argument('-v', '--verbose', action='store_true', default=False)
    parser.add_argument('--debug', action='store_true', default=False)
    subparsers = parser.add_subparsers(title='command')

    init_logging(
        debug=bool(os.getenv('CHOCOSPOOF_DEBUG')),
        filename=os.getenv('CHOCOSPOOF_LOGFILE'),
    )

    # Binding subparser
    ping.add_subparser(subparsers)
    pingscan.add_subparser(subparsers)
    dnsspoof.add_subparser(subparsers)
    arpspoof.add_subparser(subparsers)
    arpunspoof.add_subparser(subparsers)
    arpmitm.add_subparser(subparsers)

    # argcomplete.autocomplete(parser)
    try:
        args = parser.parse_args()
        if not args.debug:
            if args.verbose:
                enable_verbose()
        else:
            enable_debug()
        sys.exit(args.command(args))
    except Exception as e:
        logger.error(e)
