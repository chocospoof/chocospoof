# coding: utf8

import logging


_FORMAT_DEBUG = "%(asctime)s %(name)s %(levelname)s %(message)s"
_FORMAT = "%(asctime)s %(message)s"
_DATEFMT = "%d-%m-%Y %M:%H:%S"


def _reset_stream_handler(level: int,
                          fmt: str = _FORMAT,
                          datefmt: str = _DATEFMT):
    logger = logging.getLogger()
    for handler in logger.handlers:
        if isinstance(handler, logging.StreamHandler):
            logger.removeHandler(handler)
    handler = logging.StreamHandler()
    handler.setFormatter(logging.Formatter(fmt=fmt, datefmt=datefmt))
    logger.setLevel(level)
    logger.addHandler(handler)


def init_logging(debug=False, filename=None, level=logging.INFO):
    """ Configure logging LEVEL from environment variables
        It will be override later by parsed arguments

        Args:
            debug:      Enable DEBUG mode
            filename:   If set log to the specify path.
                        The path will be created if not exists

        Raises:
            OSError:    Failed to access the log file
    """
    if debug:
        logger_format = _FORMAT_DEBUG
        logger_level = logging.DEBUG
    else:
        logger_format = _FORMAT
        logger_level = logging.WARNING
    _reset_stream_handler(logger_level, logger_format, _DATEFMT)


def enable_verbose():
    """ Enable verbose mode, only if DEBUG is not enabled
    """
    logger = logging.getLogger()
    if logger.getEffectiveLevel() is not logging.DEBUG:
        _reset_stream_handler(logging.INFO, fmt=_FORMAT_DEBUG)


def enable_debug():
    """ Enable debug mode
    """
    _reset_stream_handler(logging.DEBUG, fmt=_FORMAT_DEBUG)


def shutdown():
    return logging.shutdown()
