# coding: utf8

import os
# from sys import exit, stderr
from multiprocessing import Process
# from tempfile import NamedTemporaryFile


def daemonize(func: callable):
    '''Asynchronously calls a function in a detached process.
       In fact, this spawn an orphan python process running the given function.
       Can be used as a normal function call or with a decorator.

        Args:
            func: Function to call in a detached process

        Returns:
            wrapper: a callable that wrap the original command

        Examples:

            1. As a decorator
                >>> @daemonize
                    def sleepAndPrint(message):
                        time.sleep(10)
                        print(message)
                    sleepAndPrint('Orphan process')

            2. Classic function call
                >>> def sleepAndPrint(message):
                        time.sleep(10)
                        print(message)
                >>> daemonize(sleepAndPrint)('Orphan process')
    '''
    # create a process fork and run the function
    def forkify(*args, **kwargs):
        if os.fork() != 0:
            return

        # run the function
        func(*args, **kwargs)

    # wrapper to run the forkified function
    def wrapper(*args, **kwargs):
        proc = Process(target=lambda: forkify(*args, **kwargs))
        proc.start()
        proc.join()
        return

    return wrapper
