# coding: utf8

import logging

import scapy.all as scapy

from .arprequest import arprequest
from chocospoof.validators import validate_ipv4

logger = logging.getLogger(__name__)


def arpunspoof(victim: str, gateway: str):
    ''' Undo an ARP spoof attack.
        This is done by... spoofing. We spoof the IP of the real gateway
        to issue an ARP broadcast, meant to be received by the victim.
        Thus the victim will send a normal ARP reply to the real gateway,
        re-establishing the truth.

        Args:
            victim: IPv4 address of the victim
            gateway: Source IPv4 address to spoof

        Returns:
            True if the ARP broadcast has been send.

        Examples:
            Undo spoofing of 172.18.0.254 on victim 172.18.0.1 :

            >>> arpunspoof('172.18.0.1', '172.18.0.254')
            True
    '''
    # Get MAC of gateway
    MAC_gateway = arprequest(gateway)

    if MAC_gateway is not None:
        # Craft ARP frame : for the victim
        arp = scapy.ARP()
        arp.op = 'who-has'
        # Spoofing the gateway address
        arp.pdst = victim
        arp.psrc = gateway
        arp.hwdst = "ff:ff:ff:ff:ff:ff"
        arp.hwsrc = MAC_gateway
        # Send spoofed ARP frame, re-establishing the truth for victim
        result, unans = scapy.sr(arp, timeout=2, verbose=0)

    else:
        raise Exception(f"Gateway MAC address not found."
                        f"Target {gateway} did not respond to ARP")


def _execute(args):
    try:
        arpunspoof(str(args.target), str(args.gateway))
        logger.info('Truth re-established.\n')
    except Exception:
        raise


def add_subparser(subparsers):
    parser = subparsers.add_parser('arpunspoof', help='Re-establish truth for given target.')  # noqa: E128,E501
    parser.add_argument('target', type=validate_ipv4,
                        help='Target IP (example: 192.168.1.55).')
    parser.add_argument('-g', '--gateway', type=validate_ipv4,
                        help='Gateway IP to spoof.')
    parser.set_defaults(command=_execute)
