# coding: utf8


import logging

import scapy.all as scapy

from chocospoof.validators.args import validate_ipv4


logger = logging.getLogger(__name__)


def ping(host: str, ping_timeout: int = 2, ping_ttl: int = 2):
    ''' Send simple ICMP packet with raw sockets to a single IPv4 address.

        Args:
            ttl: ICMP Time to Live (number of hops)
            ping_timeout: ICMP timeout (in seconds)

        Returns:
            If the host answered to ICMP packet, return True.

        Examples:
            Ping localhost

            >>> ping('127.0.0.1', ping_timeout=2, ping_ttl=2)
            True
    '''
    # Configure scapy to be used with local sockets
    # TODO: Check if tcpdump is available (failed to used layer)
    scapy.conf.L3socket
    scapy.conf.L3socket = scapy.L3RawSocket

    # Craft packet
    packet = scapy.IP(dst=host, ttl=ping_ttl)/scapy.ICMP()
    # Send packet
    reply = scapy.sr1(packet, timeout=ping_timeout, verbose=False)

    return reply is not None


def _execute(args):
    if ping(str(args.target)):
        logger.info(f'Host {args.target} is up')
    return 0


def add_subparser(subparsers):
    parser = subparsers.add_parser('ping', help='simple ping')
    parser.add_argument('target', type=validate_ipv4,
                        help='IP (example: 127.0.0.1)')
    parser.set_defaults(command=_execute)
