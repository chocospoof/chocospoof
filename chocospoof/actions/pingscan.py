# coding: utf8

import logging
from multiprocessing import Pool
from ipaddress import IPv4Network
from typing import List

from chocospoof.validators.args import validate_ipv4_cidr
from .ping import ping
from .arprequest import arprequest


DEFAULT_TTL = 10
DEFAULT_TIMEOUT = 2
DEFAULT_MAX_PROCESSES = 16


logger = logging.getLogger(__name__)


def pingscan(network: str, ping_timeout: int, ttl: int,
             maxProcesses: int) -> List[str]:
    ''' Send simple ICMP packets with raw sockets to a whole IPv4 network.

        Args:
            network: IPv4 address of network to scan, CIDR formatted
                     Example : '192.168.1.0/24'
            ttl: ICMP Time to Live (number of hops)
            ping_timeout: ICMP timeout (in seconds)
            maxProcesses: number of processes to run in parallel

        Returns:
            List of hosts that answered to ICMP packet.

        Examples:
            Scan 172.18.0.0/30

            >>> pingScan('172.18.0.0/30', ping_timeout=2, ping_ttl=2)
            ['172.18.0.1', '172.18.0.3']

            Scan 172.18.0.0/30 with max parocess set

            >>> pingScan('172.18.0.0/30', maxProcesses=8)
            ['172.18.0.1', '172.18.0.3']
    '''
    hosts = [str(x) for x in IPv4Network(network).hosts()]
    hosts_status = []
    hosts_up = []
    # Scan the network using Python multiprocessing
    with Pool(processes=maxProcesses) as pool:
        hosts_status = dict(zip(hosts, pool.map(ping, hosts)))
        for host, status in hosts_status.items():
            if status:
                hosts_up.append(host)
        hosts_up = dict(zip(hosts_up, pool.map(arprequest, hosts_up)))
    return hosts_up


def _execute(args):
    for host, mac in pingscan(str(args.target),
                              args.timeout,
                              args.ttl,
                              args.maxprocesses).items():
        logger.info(f'{host} is up {mac}')
    return 0


def add_subparser(subparsers):
    parser = subparsers.add_parser('pingscan',
                        help='ping a whole network')   # noqa: E128
    parser.add_argument('target', type=validate_ipv4_cidr,
                        help='Network CIDR (example: 192.168.1.0/24)')
    parser.add_argument('--timeout',
                        type=int, default=DEFAULT_TIMEOUT,
                        help=f"Frame timeout (in seconds)."
                        f" Defaults to %(default)s.")
    parser.add_argument('--ttl', type=int, default=DEFAULT_TTL,
                        help=f"Frame time to live (in number of hops)."
                        f"Defaults to %(default)s.")
    parser.add_argument('-M', '--maxprocesses', type=int,
                        default=DEFAULT_MAX_PROCESSES,
                        help="Max parallel processes"
                        f'''(only with -a pingscan.
                            Defaults to %(default)s.''')
    parser.set_defaults(command=_execute)
