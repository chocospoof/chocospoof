import scapy.all as scapy


def arprequest(ip: str) -> str:
    ''' Determine MAC address of given IPv4 using ARP request.

        Args:
            ip: Target IPv4

        Returns:
            MAC address of target IP if target has answered ARP request.
            None if the target did not answered.

        Examples:
            Valid host : get MAC address of 192.168.1.1

            >>> getMacAddress('192.168.1.1')
            '76:89:4c:f7:0a:bd'

            Invalid host : get MAC address of 192.168.1.2

            >>> getMacAddress('192.168.1.2')
            None
    '''
    # Craft ARP frame : classic ARP request
    arp = scapy.ARP()
    arp.op = 1
    arp.pdst = ip
    arp.hwdst = "ff:ff:ff:ff:ff:ff"

    # Send ARP request and store ARP reply
    # It contains the MAC address of the target as the MAC source
    resp, unans = scapy.sr(arp, retry=2, timeout=2, verbose=False)

    # Extract MAC address from received ARP reply and return it
    for s, r in resp:
        return r[scapy.ARP].hwsrc

    return None
