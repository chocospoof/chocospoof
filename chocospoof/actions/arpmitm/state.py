# coding: utf8

from argparse import ArgumentParser
import time
import psutil
from os import getpid, stat
import logging
import json
import re

global_state_file = '/tmp/chocospoof-arpmitm.state'
main_package_name = __name__.split('.')[0]

logger = logging.getLogger(__name__)


def get():
    ''' Get informations about currently running ARP MITM attacks.
        Informations are retrieved from state file on disk.

        Returns:
            Python dict containing all running MITM attacks

        Examples:
            get() While two attacks are running :

            >>> print(json.dumps(get()), indent=2)
            {
              "37892": {
                "timestamp": "Sat Mar 21 22:33:39 2020",
                "victim": "192.168.1.29",
                "gateway": "192.168.1.1"
              },
              "18373": {
                "timestamp": "Sat Mar 21 22:38:46 2020",
                "victim": "192.168.1.35",
                "gateway": "192.168.1.1"
              }
            }
    '''
    current_state = {}
    try:
        with open(global_state_file, 'r') as json_file:
            # Only json.load the file if it's not empty
            if stat(global_state_file).st_size != 0:
                current_state = json.load(json_file)
            # When state is an empty json, json.load force return a list
            # We need a dict
            if isinstance(current_state, list):
                current_state = dict(zip(current_state))
    except FileNotFoundError:
        logger.info(f"State file {global_state_file} does not exist.")
        pass
    except OSError:
        logger.error(f"Unable to read file {global_state_file}.")
        raise
    except Exception as e:
        logger.error(f"Unexpected error : \n{e}")
        raise

    return current_state


def refresh(current_state):
    ''' Refresh ARP MITM global state with new global state

        Args:
            current_state: dict containint new MITM state

        Returns:
            True when refresh has been successful

        Examples:
            Print a new state dict and refresh MITM state

            >>> print(json.dumps(new_state))
            {
              "37892": {
                "timestamp": "Sat Mar 21 22:33:39 2020",
                "victim": "192.168.1.29",
                "gateway": "192.168.1.1"
              }
            }
        >>> refresh(new_state)
        True
    '''
    current_state_json = json.dumps(current_state)
    try:
        # Write new MITM state to disk as json
        with open(global_state_file, 'w') as f:
            logger.info(f"Refresh {global_state_file} state")
            f.write(current_state_json)
    except OSError:
        logger.error(f"Unable to write file {global_state_file}.")
        logger.error(f"MITM state {global_state_file}"
                     f"might be inconsistent.")
        raise
    except Exception as e:
        logger.error(f"Unexpected error : \n{e}")
        logger.error(f"MITM state file {global_state_file}"
                     f"might be inconsistent.")
        raise

    return True


def add(arpmitm_pid: int, victim: str, gateway: str):
    ''' Add new ARP MITM state to current global state

        Args:
            arpmitm_pid: PID of the ARP MITM attack
            victim: IPv4 address of the victim
            gateway: IPv4 address of the gateway

        Returns:
            True if the new state has been successfully added

        Examples:
            Add new state :

            >>> add('3012', '192.168.1.101', '192.168.1.1.')
            True
    '''
    current_state = get()

    new_arpmitm = {}
    new_arpmitm[arpmitm_pid] = {}
    new_arpmitm[arpmitm_pid]['timestamp'] = time.strftime("%c")
    new_arpmitm[arpmitm_pid]['victim'] = victim
    new_arpmitm[arpmitm_pid]['gateway'] = gateway

    current_state.update(new_arpmitm)

    logger.info(f"Add PID {arpmitm_pid} to global state {global_state_file}")
    refresh(current_state)
    return True


def remove(arpmitm_pid: int):
    ''' Remove ARP MITM state from current global state

        Args:
            arpmitm_pid: PID of the ARP MITM attack to remove

        Returns:
            True if the new state has been successfully removed

        Examples:
            Remove a state :

            >>> remove('3012')
            True
    '''
    current_state = get()
    current_state.pop(str(arpmitm_pid), None)
    refresh(current_state)
    logger.info(f"PID {arpmitm_pid} removed "
                f"from global state {global_state_file}\n")
    return True


def remove_self():
    ''' Remove ARP MITM attack state corresponding to current PID
        from global state

        Returns:
            True if the new state has been successfully removed

        Examples:
            Remove

            >>> remove_self()
            True
    '''
    current_state = get()
    arpmitm_pid = getpid()
    current_state.pop(str(arpmitm_pid), None)
    logger.info(f"Remove self PID {arpmitm_pid} "
                f"from global state {global_state_file}")
    refresh(current_state)
    return True


def parse_cmdline(cmdline: list):
    ''' Parse processes' cmdline with Argparse
        Used to retrieve informations about currently running ARP MITM attacks.

        Args:
            cmdline: cdline of a given process, as a list of strings

        Returns:
            args: Argparse object

        Examples:
            Parse a crafted cmdline

            >>> parse_cmdline({'sudo', 'env/bin/chocospoof', 'arpmitm', '192.168.1.15', '-g', '192.168.1.1'})  # noqa: E501
            True
    '''

    # Get index of command in cmdline
    chocospoof_idx = [i for i, item in enumerate(cmdline)
                      if item.endswith(main_package_name)][0]

    # Strip cmdline to only contain the 'chocospoof' command
    # This is needed when using prefix to main 'chocospoof' command
    # Like usage of 'sudo' : sudo chocospoof ...
    cmdline = cmdline[chocospoof_idx:]

    # Parse cmdline as a real chocospoof command line call
    parser = ArgumentParser(prog=main_package_name)
    subparsers = parser.add_subparsers(title='command')
    arpmitm_parser = subparsers.add_parser('arpmitm')
    arpmitm_parser.add_argument('target', type=str)
    arpmitm_parser.add_argument('-g', '--gateway', type=str)
    args, unknown = parser.parse_known_args()

    return args


def get_running_attacks_pids():
    ''' Get running ARP MITM attacks PIDs.
        This is done by parsing current machine processes using Python's psutil
        and grabbing those who are :

            * using the chocospoof binary
            * using the arpmitm attack
            * orphan (PPID of 1)

        Returns:
            running_arp_mitm_pids: dict containing :

                * PID of the process
                * victim of the attack
                * gateway used for the attack

        Examples:
            While two ARP MITM process are running :

            >>> print(json.dumps(get_running_attacks_pids()), indent=2)
            {
            "89657": {
              "timestamp": "Sun Mar 22 02:33:06 2020",
              "victim": "192.168.1.29",
              "gateway": "192.168.1.1"
            },
            "90285": {
              "timestamp": "Sun Mar 22 02:34:13 2020",
              "victim": "192.168.1.29",
              "gateway": "192.168.1.1"
              }
            }
    '''
    # Get all running arpmitm PIDs
    running_arp_mitm_pids = {}
    current_pid = getpid()
    for pid in psutil.pids():
        try:
            process = psutil.Process(pid)
        except psutil.NoSuchProcess:
            continue
        if pid == current_pid:
            continue

        process_cmdline = process.cmdline()
        cmdline_re = f"^.*{main_package_name} .* arpmitm.*$"
        if (re.match(cmdline_re, ' '.join(process_cmdline)) and
                process.ppid() == 1):
            args = parse_cmdline(process_cmdline)
            running_arp_mitm_pids[pid] = \
                {"victim": args.target, "gateway": args.gateway}

    return running_arp_mitm_pids


def check_consistency():
    ''' Check consistency of global state {global_state_file}.
        Find untracked ARP MITM attacks (currently running, but not registered)
        and register them.
        Find dead ARPM MITM attacks (registered but not running)
        and remove them.

        Returns:
            True if the check has succeeded.

        Examples:
            >>> check_consistency()
            True
    '''
    logger.info(f"Run consistency check. "
                f"Using global state from {global_state_file}.")

    # Get running attacks PIDs
    running_arp_mitm_pids = get_running_attacks_pids()

    # Get presumed state of running arpmitm processes from global state
    state_arp_mitm_pids = get()

    # Get PIDs as lists
    state_arp_mitm_pids_list = list(state_arp_mitm_pids.keys())
    state_arp_mitm_pids_list = [int(i) for i in state_arp_mitm_pids_list]
    running_arp_mitm_pids_list = list(running_arp_mitm_pids.keys())

    # Get consistent PIDs, those which are in both list
    consistent_pids = list(set(state_arp_mitm_pids_list) &
                           set(running_arp_mitm_pids_list))

    # When removing consistent PIDs from their original lists, we got :
    #  - registered processes in global state but not running : dead
    #  - running processes but not registered in global state : untracked
    untracked_arp_mitm_pids = list(set(running_arp_mitm_pids_list) -
                                   set(consistent_pids))
    dead_arp_mitm_pids = list(set(state_arp_mitm_pids_list) -
                              set(consistent_pids))

    logger.info(f"Processes: ")
    logger.info(f"  Running : {str(running_arp_mitm_pids_list)}")
    logger.info(f"  Global state : {str(state_arp_mitm_pids_list)}")
    logger.info(f"  Consistent : {str(consistent_pids)}")
    logger.info(f"  Untracked : {str(untracked_arp_mitm_pids)}")
    logger.info(f"  Dead : {str(dead_arp_mitm_pids)}")

    # Add untracked processes to global state
    if untracked_arp_mitm_pids:
        for pid in untracked_arp_mitm_pids:
            logger.info(f"Consistency check : "
                        f"Adding untracked PID {str(pid)} to global state")
            add(pid,
                running_arp_mitm_pids[pid]['victim'],
                running_arp_mitm_pids[pid]['gateway'])

    # Remove dead processes from global state
    if dead_arp_mitm_pids:
        for pid in dead_arp_mitm_pids:
            logger.info(f"Consistency check : "
                        f"Removing dead PID {str(pid)} from global state")
            remove(pid)

    return True
