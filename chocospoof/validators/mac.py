import re


def isMACAddress(mac: str) -> bool:
    ''' Determine if given arg is a valid MAC address.

        Args:
            mac: MAC address to test

        Returns:
            True if the submitted MAC address is valid.

        Examples:
            Test 76:89:4c:f7:0a:bd

            >>> isMACAddress('76:89:4c:f7:0a:bd')
            True
    '''
    mac_address_regex = "^[0-9a-f]{2}([-:]?)[0-9a-f]{2}(\\1[0-9a-f]{2}){4}$"
    return re.match(mac_address_regex, mac.lower())
