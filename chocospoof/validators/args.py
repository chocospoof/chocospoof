import logging
import argparse
from ipaddress import IPv4Address, IPv4Network
from . import isMACAddress, isValidDomainName


logger = logging.getLogger(__name__)


def validate_ipv4(ipv4):
    try:
        IPv4Address(ipv4)
    except Exception as e:
        raise argparse.ArgumentTypeError(e)
    return ipv4


def validate_ipv4_cidr(ipv4_cidr):
    try:
        IPv4Network(ipv4_cidr)
    except Exception as e:
        raise argparse.ArgumentTypeError(e)
    return ipv4_cidr


def validate_mac(mac):
    if not isMACAddress(mac):
        raise argparse.ArgumentTypeError("Invalid MAC address")
    return mac


def validate_domain_name(domain):
    if not isValidDomainName(domain):
        logger.warning(f"Malformed domains {domain}")
    else:
        return domain
