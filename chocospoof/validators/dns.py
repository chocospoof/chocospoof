# coding: utf-8

import re


def isValidDomainName(domain: str) -> bool:
    """ Determine if given string is a valid domain name

        Args:
            domain: Domain name

        Returns:
            True if the submitted domain name is valid

        Examples:
            Test ``google.com``

            >>> isValidDomainName('google.com')
            True

        References:
            Domain name validation: https://regexr.com/3au3g
    """
    valid_domain_name = re.compile(
        r'''(?:
                [a-z0-9]
                (?:
                    [a-z0-9-]{0,61}
                    [a-z0-9]
                )?\.
            )
            +
            [a-z0-9][a-z0-9-]{0,61}
            [a-z0-9]
        ''',
        re.X)
    return valid_domain_name.match(domain)
