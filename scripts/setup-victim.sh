#!/bin/bash
# vi: set expandtab shiftwidth=4 softtabstop=4

yum install -y tcpdump bind-utils

ip route show default | grep default | grep -q '192.168.254.254' \
    || ip route del default \
    && ip route add default via '192.168.254.254' dev eth1

sed -i.updated -e 's#nameserver [[:print:]]*#nameserver 192.168.254.254#g' /etc/resolv.conf
