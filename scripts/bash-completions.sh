#!/bin/sh
# vi: set expandtab shiftwidth=4 softtabstop=4

SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
PYTHON3_VENV="${SCRIPTPATH}/../venv"
PYTHON3_INTERPRETER="${SCRIPTPATH}/../venv/bin/python3"

[[ ! -d "${PYTHON3_VENV}" ]] && echo -e "python3 venv not detected in ${PYTHON3_VENV}\nRun 'make install' first."
[[ ! -f "${PYTHON3_INTERPRETER}" ]] && echo -e "python3 not detected at path ${PYTHON3_INTERPRETER}\nRun 'make install' first."

${PYTHON3_INTERPRETER} -c 'import argcomplete' || echo -e 'You need to install argcomplete: pip install -U argcomplete' && exit 1
/usr/bin/activate-global-python-argcomplete --user
grep -q 'register-python-autocomplete chocospoof' || >>$HOME/.bashrc echo -e 'eval "$(register-python-argcomplete chocospoof)"'
