#!/bin/bash
# vi: set expandtab shiftwidth=4 softtabstop=4

SOURCE='/opt/python38'

yum install -y gcc make
yum -y install openssl-devel bzip2-devel libffi-devel

if ! [[ -d ${SOURCE} ]]; then
    mkdir -p ${SOURCE}
    cd ${SOURCE}
    wget -O Python-3.8.1.tgz https://www.python.org/ftp/python/3.8.1/Python-3.8.1.tgz
    tar xvf Python-3.8.1.tgz
    cd Python-3.8.1
    ./configure --enable-optimizations
    sudo make -j4 install
fi

systemctl disable --now firewalld

/usr/local/bin/pip3 install -U pip

ip route show default | grep default | grep -q '192.168.254.254' \
    || ip route del default \
    && ip route add default via '192.168.254.254' dev eth1

/sbin/iptables -t nat -A POSTROUTING -s 192.168.254.0/24 ! -d 192.168.254.0/24 -j MASQUERADE

/sbin/sysctl -w net.ipv4.ip_forward=1
/sbin/sysctl -w net.ipv4.ip_nonlocal_bind=1

sed -i.updated -e 's#nameserver [[:print:]]*#nameserver 1.1.1.1#g' /etc/resolv.conf

grep -q 'SELINUX=enforcing' /etc/selinux/config \
    && sed -i.enabled /etc/selinux/config -e 's#SELINUX=enforcing#SELINUX=disabled#g' \
    && setenforce 0
