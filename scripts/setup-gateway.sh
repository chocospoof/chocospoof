#!/bin/bash
# vi: set expandtab shiftwidth=4 softtabstop=4

UNBOUND_CONFIG="/etc/unbound/unbound.conf"

yum install -y unbound tcpdump bind-utils

systemctl enable --now unbound
systemctl disable --now firewalld

sed -n '48p' ${UNBOUND_CONFIG} | grep -q '# interface' ${UNBOUND_CONFIG} \
    && sed -i -e '48s!# !!' ${UNBOUND_CONFIG}
sed -n '49p' ${UNBOUND_CONFIG} | grep -q '# interface' ${UNBOUND_CONFIG} \
    && sed -i -e '49s!# !!' ${UNBOUND_CONFIG}

sed -n '864,869p' ${UNBOUND_CONFIG} | grep -q '# forward-zone' ${UNBOUND_CONFIG} \
    && sed -i -e '864s!# !!' ${UNBOUND_CONFIG} \
    && sed -i -e '865s!# !!' -e '865s!example.com!.!' ${UNBOUND_CONFIG} \
    && sed -i -e '866s!# !!' -e '866s!192.0.2.67!1.1.1.1!' ${UNBOUND_CONFIG} \
    && sed -i -e '867s!# !!' -e '867s!192.0.2.73@5355!8.8.8.8!' ${UNBOUND_CONFIG} \

sed -n '254p' ${UNBOUND_CONFIG} | grep -q '# access-control: 127.0.0.0/8 allow' \
    && sed -i -e '254s!# !!' -e '254s!127.0.0.0/8!192.168.254.0/24!' ${UNBOUND_CONFIG}

systemctl restart unbound

/sbin/iptables -t nat -A POSTROUTING -s 192.168.254.0/24 ! -d 192.168.254.0/24 -j MASQUERADE

/sbin/sysctl -w net.ipv4.ip_forward=1
/sbin/sysctl -w net.ipv4.ip_nonlocal_bind=1

grep -q 'SELINUX=enforcing' /etc/selinux/config \
    && sed -i.enabled -e 's#SELINUX=enforcing#SELINUX=disabled#g' /etc/selinux/config \
    && setenforce 0
