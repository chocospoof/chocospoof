# coding=utf8

from setuptools import find_packages, setup

setup(
    name='chocospoof',
    version='0.0.1',
    entry_points='''
        [console_scripts]
        chocospoof=chocospoof.cli:main
    ''',
    packages=find_packages(),
    install_requires=[
        "scapy==2.4.3",
        "argcomplete==1.11.1",
        "NetfilterQueue==0.8.1",
    ],
    dependency_links=[
        'git+https://github.com/kti/python-netfilterqueue#egg=NetfilterQueue-0.8.1',
    ],
    python_requires='>=3.7',
    classifiers=[
        "Development Status :: 5 - In developpment",
        "Topic :: Software Development :: Libraries :: Python Modules",
        "License :: Apache License Version 2.0",
        "Programming Language :: Python :: 3",
    ]
)
