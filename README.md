# Chocospoof

Spoof all the shit !

Gitlab CI Status | [![pipeline status](https://gitlab.com/chocospoof/chocospoof/badges/master/pipeline.svg)](https://gitlab.com/chocospoof/chocospoof/-/commits/master)
-----------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------|
Code coverage    | [![coverage report](https://gitlab.com/chocospoof/chocospoof/badges/master/coverage.svg?job=coverage)](https://gitlab.com/chocospoof/chocospoof/commits/master)

# Install

## Prerequisites

### `sysctl` config

To use all L2 actions you need to be able to forward packets.  

On GNU/Linux : 

```bash
sudo sysctl -w net.ipv4.ip_forward=1
sudo sysctl -w net.ipv4.ip_nonlocal_bind=1
```

### Packages

Arch Linux

```bash
pacman -S libnetfilter_queue
```

## Python `venv`

```bash
git clone https://gitlab.com/chocospoof/chocospoof.git
cd chocospoof
make install
venv/bin/chocospoof -h
```

## Docker

```bash
docker login registry.gitlab.com
docker pull registry.gitlab.com/chocospoof/chocospoof:latest
docker run chocospoof -h
```
