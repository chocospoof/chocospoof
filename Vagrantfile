# -*- mode: ruby -*-

Vagrant.configure("2") do |config|
  config.ssh.insert_key = false
	config.vm.synced_folder ".", "/vagrant", disabled: true

  config.vm.provider "libvirt" do |libvirt|
    libvirt.cpus = 1
    libvirt.memory = 512
    # Fix the issue on Fedora 31:
    # Network 192.168.5.10 is not available. Specify available network
    # name, or an ip address if you want to create a new network.
    #
    # https://github.com/vagrant-libvirt/vagrant-libvirt/issues/958
    libvirt.qemu_use_session = false
  end

  config.vm.define "gateway" do |vb|
    vb.vm.box = "generic/centos8"
    vb.vm.hostname = "gateway"
    vb.vm.network "private_network", ip: "192.168.254.254"
    vb.vm.provision "shell", path: "scripts/setup-gateway.sh"
  end
  config.vm.define "victim" do |vb|
    vb.vm.box = "generic/centos8"
    vb.vm.hostname = "victim"
    vb.vm.network "private_network", ip: "192.168.254.10"
    vb.vm.provision "shell", path: "scripts/setup-victim.sh"
  end
  config.vm.define "attacker" do |vb|
    vb.vm.box = "generic/centos8"
    vb.vm.hostname = "attacker"
    vb.vm.provider "libvirt" do |libvirt|
      libvirt.cpus = 4
      libvirt.memory = 1024
    end
    vb.vm.network "private_network", ip: "192.168.254.5"
    vb.vm.synced_folder ".", "/vagrant", type: "rsync"
    vb.vm.provision "shell", path: "scripts/setup-attacker.sh"
  end
end

