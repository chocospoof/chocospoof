PACKAGE=chocospoof
VENV=venv
_PYTHON=`command -v python3`
_PIP=`command -v pip`

.PHONY: venv clean test docs

all:	coverage

venv:
	@test -d $(VENV) || $(_PYTHON) -m $(VENV) $(VENV)
	@$(VENV)/bin/pip install -q --upgrade pip
	@$(VENV)/bin/pip install -q -r requirements.txt

build:	venv
	@$(VENV)/bin/python setup.py build

dist:	venv
	@$(VENV)/bin/python setup.py sdist

docs:	
	$(_PIP) install -q -r requirements.txt
	make -C docs html

install: venv
	@$(VENV)/bin/python setup.py install

install-docker:
	$(_PIP) install -r requirements.txt
	$(_PYTHON) setup.py install

test:	venv install
	$(VENV)/bin/pytest -v
	$(VENV)/bin/coverage run --source $(PACKAGE) -m pytest -v

test-docker:
	/usr/local/bin/pytest -v
	/usr/local/bin/coverage run --source $(PACKAGE) -m pytest -v
	/usr/local/bin/coverage json -o .coverage.json --pretty-print

lint:
	$(VENV)/bin/flake8 tests
	$(VENV)/bin/flake8 $(PACKAGE)

lint-docker:
	/usr/local/bin/flake8 tests
	/usr/local/bin/flake8 $(PACKAGE)
	
coverage: test
	@$(VENV)/bin/coverage json -o .coverage.json --pretty-print
	$(VENV)/bin/coverage report -m

coverage-docker:
	/usr/local/bin/coverage json -o .coverage.json --pretty-print
	/usr/local/bin/coverage report -m

clean:
	@-rm -fr dist
	@-rm -fr build
	@-find . -type d -name __pycache__ -execdir rm -fr {} +
	@-find . -type d -name *.egg-info -execdir rm -fr {} +
	@-find . -type d -name *.pyc -execdir rm -f {} +
	@-rm -fr .pytest_cache
	@-rm -f .coverage*

fclean:	clean
	@-rm -fr $(VENV)
