FROM python:3.8-buster

RUN apt-get update -y
RUN apt-get install -y git libnfnetlink-dev libnetfilter-queue-dev

COPY Makefile setup.py requirements.txt /app/
COPY chocospoof /app/chocospoof
COPY tests /app/tests

WORKDIR /app

RUN /usr/local/bin/python3 setup.py install \
    && /usr/bin/make install-docker

ENTRYPOINT [ "/usr/local/bin/chocospoof" ]
CMD [ "-h" ]
