# coding: utf8


import pytest

from chocospoof.actions.pingscan import pingscan
from chocospoof.actions.ping import ping
from chocospoof.actions.arprequest import arprequest


@pytest.fixture
def mock_map():
    def map(self, func, hosts):
        if func == ping:
            for x in hosts:
                yield True
        if func == arprequest:
            for x in hosts:
                yield None
    return map


def test_pingscan(mock_map, monkeypatch):
    monkeypatch.setattr('multiprocessing.pool.Pool.map', mock_map)
    assert pingscan('127.0.0.0/30', 2, 10, 16) == {
            '127.0.0.1': None,
            '127.0.0.2': None,
            }
