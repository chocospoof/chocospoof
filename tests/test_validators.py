# coding: utf8

import unittest
import argparse

from chocospoof.validators import (
    validate_ipv4,
    validate_ipv4_cidr,
    validate_domain_name,
    validate_mac,
    isMACAddress,
    isValidDomainName,
)


class TestValidators(unittest.TestCase):
    def test_valid_mac_address(self):
        self.assertTrue(isMACAddress('76:89:4c:f7:0a:bd'))
        self.assertTrue(isMACAddress('ff:ff:ff:ff:ff:ff'))
        self.assertTrue(isMACAddress('00:00:00:00:00:00'))

    def test_invalid_mac_address(self):
        self.assertFalse(isMACAddress(':89:4c:f7:0a:bd'))
        self.assertFalse(isMACAddress('76:111:4c:f7:0a:bd'))
        self.assertFalse(isMACAddress('76:89:4c:Z7:0a:bd'))

    def test_valid_domain_name(self):
        self.assertTrue(isValidDomainName('google.com'))
        self.assertTrue(isValidDomainName('www.google.com'))
        self.assertTrue(isValidDomainName('ti.tu.to.ta'))

    def test_invalid_domain_name(self):
        self.assertFalse(isValidDomainName('76:89:4c:f7:0a:bd'))
        self.assertFalse(isValidDomainName('122.1.1.1.1'))
        self.assertFalse(isValidDomainName(''))
        self.assertFalse(isValidDomainName('.'))
        self.assertFalse(isValidDomainName('titi'))

    def test_args_validate_ipv4_exception(self):
        with self.assertRaises(argparse.ArgumentTypeError):
            validate_ipv4('55:55')

    def test_args_validate_domain_name_exception(self):
        self.assertEqual(validate_domain_name('zti'), None)

    def test_args_validate_mac_exception(self):
        with self.assertRaises(argparse.ArgumentTypeError):
            validate_mac('::')

    def test_args_validate_ipv4_cidr_exception(self):
        with self.assertRaises(argparse.ArgumentTypeError):
            validate_ipv4_cidr('127.0.0.1//23')

    def test_args_validate_ipv4(self):
        self.assertEqual(validate_ipv4('127.0.0.1'), '127.0.0.1')

    def test_args_validate_domain_name(self):
        self.assertEqual(validate_domain_name('titi.fr'), 'titi.fr')

    def test_args_validate_mac(self):
        self.assertEqual(validate_mac('76:89:4c:f7:0a:bd'), '76:89:4c:f7:0a:bd') # noqa E501

    def test_args_validate_ipv4_cidr(self):
        self.assertEqual(validate_ipv4_cidr('127.0.0.0/30'), '127.0.0.0/30')
