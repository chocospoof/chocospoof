# coding: utf8


import pytest

from chocospoof.actions.dnsspoof import (
    is_ipv4_forwarding_enable,
    # is_iptables_available,
    # is_nfqueue_available,
    # spoof_response,
    # dnsspoof,
)


@pytest.fixture
def mock_open():
    class open():
        response = '1\n'
        error = None

        def __init__(self, filename):
            pass

        def __enter__(self):
            if self.error is not None:
                raise self.error()
            return self

        def read(self):
            return self.response

        def __exit__(self, *args):
            pass
    return open


@pytest.fixture
def mock_open_forwarding_enable(mock_open):
    return mock_open


@pytest.fixture
def mock_open_forwarding_disable(mock_open):
    mock_open.response = '\n'
    return mock_open


@pytest.fixture
def mock_open_os_error(mock_open):
    mock_open.error = OSError
    return mock_open


def test_ipv4_forwarding_enable(mock_open_forwarding_enable, monkeypatch):
    monkeypatch.setattr('builtins.open', mock_open_forwarding_enable)
    assert is_ipv4_forwarding_enable() is True


def test_ipv4_forwarding_disable(mock_open_forwarding_disable, monkeypatch):
    monkeypatch.setattr('builtins.open', mock_open_forwarding_disable)
    assert is_ipv4_forwarding_enable() is False


def test_ipv4_forwarding_exception(mock_open_os_error, monkeypatch):
    monkeypatch.setattr('builtins.open', mock_open_os_error)
    with pytest.raises(OSError):
        is_ipv4_forwarding_enable()
