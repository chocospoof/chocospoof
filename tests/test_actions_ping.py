# coding: utf8

import unittest
from unittest.mock import patch

import scapy

from chocospoof.actions.ping import ping


class TestActionPing(unittest.TestCase):
    @patch.object(scapy.all.conf, 'L3socket', return_value=None)
    @patch.object(scapy.all, 'sr1', return_value='')
    def test_ping_loopback(self, L3socket, sr1):
        self.assertTrue(ping('127.0.0.1'))

    @patch("scapy.all.sr1", return_value=None)
    def test_ping_no_reply(self, sr1):
        self.assertFalse(ping('192.168.11.1'))
